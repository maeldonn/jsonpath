# JSON Path

_JSONPath is a generic function in Go that allows you to extract a value from a JSON document using a specified path. The function supports different return types, including int, string, float64 and bool._

## Usage

```go
package main

import (
	"encoding/json"
	"log"
	"reflect"

	"gitlab.com/maeldonn/jsonpath"
)

func main() {
	jsonStr := `{
        "a1": {
            "b1": {
                "c": "cValue"
            },
            "b2": 2
        },
        "a2": "aValue"
    }`

	var jsonObject any
	if err := json.Unmarshal([]byte(jsonStr), &jsonObject); err != nil {
		log.Fatal(err)
	}

	c, err := jsonpath.JSONPath[string]("a1.b1.c", jsonObject)
	if err != nil {
		log.Fatal(c)
	}

	b2, err := jsonpath.JSONPath[int]("a1.b2", jsonObject)
	if err != nil {
		log.Fatal(c)
	}

	if !reflect.DeepEqual(c, "cValue") {
		log.Fatalf("expected c to be equal to cValue, but got %s", c)
	}

	if !reflect.DeepEqual(b2, 2) {
		log.Fatalf("expected b2 to be equal to 2, but got %s", b2)
	}
}
```

## Running the Tests

Test:
```bash
$ go test
```

Benchmark:
```bash
$ go test -bench=. -count=5
```

## Resources

- [Go](https://go.dev/)
- [JSON Specification](https://www.json.org/)