package jsonpath

import (
	"encoding/json"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_JSONPath(t *testing.T) {
	type args struct {
		key  string
		json string
	}

	type testcase[T any] struct {
		args      args
		want      T
		assertErr assert.ErrorAssertionFunc
		errStr    string
	}

	tests := map[string]testcase[any]{
		"ERROR: Invalid JSON": {
			args: args{
				key: "a",
				json: `[
					["a","b"],
					12,
					"c"
				]`,
			},
			assertErr: assert.Error,
			errStr:    "node [[a b] 12 c] of type []interface {} is not an object node",
		},
		"ERROR: Key not existing": {
			args: args{
				key: "a.c",
				json: `{
					"a": {
						"b": "bValue"
					},
					"c": "cValue"
				}`,
			},
			assertErr: assert.Error,
			errStr:    "key c is not existing in node map[b:bValue]",
		},
		"ERROR: Invalid path": {
			args: args{
				key: "a.b.c",
				json: `{
					"a": {
						"b": "bValue"
					},
					"c": "cValue"
				}`,
			},
			assertErr: assert.Error,
			errStr:    "node bValue of type string is not an object node",
		},
		"ERROR: Cannot cast": {
			args: args{
				key: "a.b",
				json: `{
					"a": {
						"b": "bValue"
					},
					"c": "42"
				}`,
			},
			want:      42,
			assertErr: assert.Error,
			errStr:    "cannot cast node value of type string to int",
		},
		"SUCCESS: String": {
			args: args{
				key: "a.b.c",
				json: `{
					"a": {
						"b": {
							"c": "cValue"
						}
					},
					"d": "dValue"
				}`,
			},
			want:      "cValue",
			assertErr: assert.NoError,
		},
		"SUCCESS: Integer": {
			args: args{
				key: "a.b.c",
				json: `{
					"a": {
						"b": {
							"c": 42
						}
					},
					"d": "dValue"
				}`,
			},
			want:      42,
			assertErr: assert.NoError,
		},
		"SUCCESS: Float": {
			args: args{
				key: "a.b.c",
				json: `{
					"a": {
						"b": {
							"c": 4.2
						}
					},
					"d": "dValue"
				}`,
			},
			want:      4.2,
			assertErr: assert.NoError,
		},
		"SUCCESS: Bool": {
			args: args{
				key: "a.b.c",
				json: `{
					"a": {
						"b": {
							"c": true
						}
					},
					"d": "dValue"
				}`,
			},
			want:      true,
			assertErr: assert.NoError,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			var jsonObject any
			if err := json.Unmarshal([]byte(tt.args.json), &jsonObject); err != nil {
				t.Fatal(err)
			}

			var (
				got any
				err error
			)

			switch tt.want.(type) {
			case int:
				got, err = JSONPath[int](tt.args.key, jsonObject)
			case float64:
				got, err = JSONPath[float64](tt.args.key, jsonObject)
			case bool:
				got, err = JSONPath[bool](tt.args.key, jsonObject)
			default:
				got, err = JSONPath[string](tt.args.key, jsonObject)
			}

			tt.assertErr(t, err)
			if err != nil {
				assert.Equal(t, err.Error(), tt.errStr)
				return
			}

			assert.Equal(t, got, tt.want)
		})
	}
}

func Benchmark_JSONPath(b *testing.B) {
	f, err := os.Open("misc/example.json")
	if err != nil {
		b.Fatal(err)
	}

	var (
		data any
		path string = "key_2_level_0.key_1_level_1.key_1_level_2.value"
	)

	if err := json.NewDecoder(f).Decode(&data); err != nil {
		b.Fatal(err)
	}

	if err := f.Close(); err != nil {
		b.Fatal(err)
	}

	for range b.N {
		if _, err := JSONPath[string](path, data); err != nil {
			b.Fatal(err)
		}
	}
}
