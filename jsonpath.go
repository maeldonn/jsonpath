package jsonpath

import (
	"fmt"
	"strings"
)

// JSONTypes are JSON types
type JSONTypes interface {
	string | int | float64 | bool
}

// JSONPath allow to find a value in a json object
func JSONPath[T any](path string, json any) (T, error) {
	for _, key := range strings.Split(path, ".") {
		node, ok := json.(map[string]any)
		if !ok {
			return *new(T), fmt.Errorf("node %[1]v of type %[1]T is not an object node", json)
		}

		json, ok = node[key]
		if !ok {
			return *new(T), fmt.Errorf("key %s is not existing in node %v", key, node)
		}
	}

	// JSON numbers are decoded as float64
	switch any(*new(T)).(type) {
	case int:
		switch value := json.(type) {
		case float64:
			return any(int(value)).(T), nil
		}
	}

	if value, ok := json.(T); ok {
		return value, nil
	}
	return *new(T), fmt.Errorf("cannot cast node value of type %T to %T", json, *new(T))
}
